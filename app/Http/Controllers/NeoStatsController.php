<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
class NeoStatsController extends Controller
{
    private $fromDate;
    private $toDate;
    public function create(Request $request)
    {
        return view('neoStats');
    }
    public function store(Request $request)
    {
        // dd( $request);
        // $request->validate([
        // 'fromdate' => 'required',
        // 'todate' => 'required',
        // ]);
        $this->fromDate   = $request->fromdate;
        $this->toDate     = $request->todate;
        // $request->fromdate = "2022-02-09";
        // $request->todate = "2022-02-16";
        // dd($request);
        // die();
        return redirect()->to('getChartData');
    }
    public function getChartData(){
        $fromDate   = $this->fromDate;
        $toDate     = $this->toDate;
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.nasa.gov/neo/rest/v1/feed?start_date=$fromDate&end_date=$toDate&api_key=DEMO_KEY",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        // echo $response;
       // echo "<pre>";
        $res_neo = json_decode($response,true);
        // print_r($res_neo);
        // die();
        
        foreach ($res_neo['near_earth_objects'] as $key => $value) {
            $get_data_with_date[$key] = $value;
            foreach ($get_data_with_date[$key] as $data) {
                $neo_stats_arr[] = $data;
                // $neo_stats_arr[$key] = $data;

            }
        }


            $sum_min_diameter_km=0;
            $sum_max_diameter_km=0;
        foreach ($neo_stats_arr as $neo_stas) {
            $E[] = $neo_stas;
            foreach ($neo_stas['estimated_diameter'] as $estemetd_diameter => $value) {
                 
                if ($estemetd_diameter == 'kilometers') {
                    //dd($value);
                     $sum_min_diameter_km+=$value['estimated_diameter_min'];
                     $sum_max_diameter_km+=$value['estimated_diameter_max'];
                     $neo_diameter_km[] = $value;
                }



                // }
            }
//dd(count($neo_diameter_km));
            foreach ($neo_stas['close_approach_data'] as $close_data) {
                foreach ($close_data['relative_velocity'] as $relative_velocitykey => $value) {
                    if ($relative_velocitykey == 'kilometers_per_hour') {
                        $neo__stats_velocity_kmph[] = $value;
                    }
                }
                foreach ($close_data['miss_distance'] as $miss_distancekey => $value) {
                    if ($miss_distancekey == 'kilometers') {

                        $neo_stats_distance_km[] = $value;
                    }
                }
            }
        }

        $neo_stats_data_arr = array_keys($get_data_with_date);

        foreach ($neo_stats_data_arr as $key => $value) {
            $neo_stats_count_with_date[$value] = count($get_data_with_date[$value]);
        } 
        // dd($estemetd_diameter['kilometers']);
      

        arsort($neo__stats_velocity_kmph);
        // echo "Fastest Asteroid Id & Speed(in KM/Hour)" . "<br>";
        $fastestAseroid = Arr::first($neo__stats_velocity_kmph);
        $fastestAseroidkey = array_key_first($neo__stats_velocity_kmph);
        $fastestAseroidId = $neo_stats_arr[$fastestAseroidkey]['id'];
 
        asort($neo_stats_distance_km);
        $closestAseroid = Arr::first($neo_stats_distance_km);
        $closestAseroidkey = array_key_first($neo__stats_velocity_kmph);
        $closestAseroidId = $neo_stats_arr[$closestAseroidkey]['id'];
     

        $neoCountByDateArrKeys = array_keys($neo_stats_count_with_date);
        $neoCountByDateArrValues = array_values($neo_stats_count_with_date);
 
       $avgerageMinDiameterKm=$sum_min_diameter_km/count($neo_diameter_km);
        $averageMaxDiameterKm=$sum_max_diameter_km/count($neo_diameter_km);

       
        return view('chart', compact('fastestAseroidId', 'fastestAseroid', 'closestAseroidId', 'closestAseroid', 'neoCountByDateArrKeys', 'neoCountByDateArrValues','avgerageMinDiameterKm','averageMaxDiameterKm'));


    }
}
