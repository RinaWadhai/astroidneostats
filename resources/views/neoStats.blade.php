<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    
</head>
<body>
  

<form action="{{ route('save') }}" method="POST">
    {{ csrf_field() }}

<center>

      <div class="row">
    <div class="col-sm-2" style="background-color:lavender;">
        </div>
         <div class="col-sm-4" style="background-color:lavenderblush;">    
                <h1>Asteroid - Neo Stats</h1>
         </div>
  </div>

  <div class="row">
         <div class="col-sm-2" style="background-color:lavender;">
              <label for="fromdate" class="form-label">From Date:</label>
          </div>
         <div class="col-sm-4" style="background-color:lavenderblush;"> 
            <input class="datepicker form-control" type="text" name="fromdate" value="" />
        </div>
    </div>

  <div class="row">
         <div class="col-sm-2" style="background-color:lavender;">
             <label for="todate" class="form-label">To Date:</label>
         </div>
         <div class="col-sm-4" style="background-color:lavenderblush;"> 
           <input class="datepicker form-control" type="text" name="todate" value="" />
         </div>
  </div>

  <div class="row">
    <div class="col-sm-2" style="background-color:lavender;"> 
     </div>
         <div class="col-sm-4" style="background-color:lavenderblush;"> 
               <input class="btn btn-primary" type="submit" name="submit" value="submit">   
     </div>
  </div>
 
   
</center>
</form>
<script type="text/javascript">
 $(function () {
      $(".datepicker").datepicker();
  });
 </script>

</body>
</html>