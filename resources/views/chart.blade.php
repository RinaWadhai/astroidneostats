{{-- @extends('master') --}}
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/app.css')}} ">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css">
    <title>Result</title>
</head>

<body>

    <div class="row">
    <div class="col-sm-4" style="background-color:lavender;">

    <table class="table table-striped">
    <tr>
        <th>Fastest Asteroid Id & Speed(in KM/Hour)</th>
        <td> {{$fastestAseroidId . "=" . $fastestAseroid}} </td>  
    </tr>
     <tr>
        <th>Closest Asteroid Id & Distance(in KM) </th>
        <td> {{$closestAseroidId . "=" . $closestAseroid}}  </td>  
    </tr>
     <tr>
        <th>Average  Min Size  of the Asteroids in kilometers </th>
        <td> {{$avgerageMinDiameterKm}} </td>
  
    </tr>
     <tr>
        <th> Average  max Size  of the Asteroids in kilometers </th>
        <td> {{$averageMaxDiameterKm}} </td>  
    </tr>
    <tr>
        <th> <a href="/form" class="btn btn-success">Back</a></th>
        <td></td>  
    </tr>

</table>

   </div>

    <div class="col-sm-8" style="background-color:lavenderblush;">  <canvas id="myChart" width="400" height="400" style="border: solid;color: red;"></canvas></div>
  </div>
 

    <script src="{{asset('js/app.js')}} "></script>
    <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>

    <script>
        var noOfAstroids = <?php  echo json_encode($neoCountByDateArrValues); ?>;
        var astroidsAppeardate = <?php  echo json_encode($neoCountByDateArrKeys); ?>;
       //alert(astroidsAppeardate[0]);
        
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels:astroidsAppeardate,
                //labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [
                    {
                        label: '# of Asteroids',
                        // lineTension:1,
                        data: noOfAstroids,
                        //data: [12, 19, 3, 5, 2, 3],
                        backgroundColor: [
                            'rgba(255, 130, 132, 0.3)',
                            'rgba(54, 140, 235, 0.3)',
                            'rgba(255, 206, 86, 0.3)',
                            'rgba(75, 90, 192, 0.3)',
                            'rgba(153, 56, 255, 0.3)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }
                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
</body>

</html>