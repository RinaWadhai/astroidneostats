<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NeoStatsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::get('neo-stats','NeoStatsController@create')->name('neo-stats');
Route::get('form','NeoStatsController@create')->name('form');
Route::post('save','NeoStatsController@store')->name('save');
Route::get('getChartData', 'NeoStatsController@getChartData');
// Route::get('store','NeoStatsController@store')->name('store');

// Route::resource('neo-stats', NeoStatsController::class);
// Route::get('/neo-stats',  [NeoStatsController::class, 'create'])->name('neo-stats');
// Route::get('/neo-submit',  [NeoStatsController::class, 'store'])->name('neo-submit');
